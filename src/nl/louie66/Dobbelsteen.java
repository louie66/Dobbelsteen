package nl.louie66;

import java.util.Scanner;

public class Dobbelsteen {

    // Dit programma gooit een dobbelsteen totdat er een zes valt.
    // @author Rene de Leeuw

    public static void main(String[] args) {

        // Alle dobbelsteen representaties worden hier gedefinieerd
        // middels een constante array
        final String[] DOBBELSTEENWAARDE = {
                "   \n . \n   \n ",
                ".  \n   \n  .\n ",
                "  .\n . \n.  \n ",
                ". .\n . \n. .\n ",
                ". .\n   \n. .\n ",
                ". .\n. .\n. .\n "
        };

        // Standard input object
        Scanner input = new Scanner(System.in);

        // De string wordt ingelezen
        System.out.print("Welk karakter moet ik gebruiken voor het oog: ");
        String karakterOog = input.next();
        System.out.println();

        int dobbelsteenWaarde = 0;

        /* Het willekeurige getal is 0 gebaseerd en zal van 0 t/m 5 worden.
         * Bijvoorbeeld waarde 5 = 6 geworpen (dus steeds 1 hoger)
         * De array dobbelSteenWaarden index is ook 0 based dus
         * kan er met dezelfde reeks gewerkt worden
         */
        while (dobbelsteenWaarde != 5) {

            // Willekeurige waarde 0,1,2,3,4,5
            dobbelsteenWaarde = (int) (6 * Math.random());

            // De string wordt aangepast aan het gekozen karakter voor het oog
            System.out.println(DOBBELSTEENWAARDE[dobbelsteenWaarde]
                    .replace('.', karakterOog.charAt(0)));
        }
    }
}
